# Information

The code in this directory can be executed as ROOT macro within ROOT but it
can also be compiled into a normal executable.
It is needed to setup the runtime environment first.

## Run as ROOT macro

root -l WriteTree1.C
root -l WriteTree2.C
root -l ReadTree1.C
root -l ReadTree2.C

## Compile commands in GSI environment

g++ --std=c++17 -I$ROOTSYS/include/root -L$ROOTSYS/lib -lCore -lTree -lRIO -lMathCore WriteTree1.C -o WriteTree1
g++ --std=c++17 -I$ROOTSYS/include/root -L$ROOTSYS/lib -lCore -lTree -lRIO -lHist ReadTree1.C -o ReadTree1

## Compile WriteTree2.C and ReadTree2.C

Compiling theses two programs ins't possible using a simple command line.
The program needs some automatically generated code based on the class
defined in Gctrack.h. This is only possible using a build system. In this
example we use the CMake to generate the needed build files. Execute the
following command in the source directory to generate the build files

`cmake -S . -B build`

and 

`cmake --build build`

to compile and link the executables. If the compilation was successful the
generated executable WriteTree2 and ReadTree2 ar in the build directory.

 

 