#include <iostream>

int for_loop() 
{
  int a{5};

  std::cout << "Before for loop." << std::endl;
  for (int loop_counter = 0; loop_counter <= a; ++loop_counter) {
    std::cout << "loop counter is " << loop_counter << "\n";
  } 
  std::cout << "After for loop." << std::endl;

  return 0;
}

int main(int argc, char* argv[])
{
  for_loop();
  return 0;
}

