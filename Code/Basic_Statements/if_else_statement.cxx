#include <iostream>

int if_else_statement()
{
  int a{5};

  std::cout << "Before if/else statement." << std::endl;
  if (5 == a) {
    std::cout << "The variable a is equal to 5.\n";
  } 
  else {
    std::cout << "The variable a is not equal to 5.\n";
  }
  std::cout << "After if/else statement." << std::endl;

  return 0;
}

int main(int argc, char* argv[])
{
  if_else_statement();
  return 0;
}
