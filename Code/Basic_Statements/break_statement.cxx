#include <iostream>

int break_statement() 
{
  int a{20};
  int values[20] = {101, 223, 387, 475, 535, 669, 7, 8, 9, 10, 11, 12 ,13, 14, 15, 16, 17, 18, 19, 20};

  std::cout << "Check if there is an even number in the array of size " << a << std::endl;
  for (int loop_counter = 0; loop_counter <= a; ++loop_counter) {
    // exit the loop if the number 7 is found in array 
    if ( values[loop_counter]%2 == 0) break;
    std::cout << " Checked the " << loop_counter+1 << " value" << "\n";
    std::cout << "The checked value was " << values[loop_counter] << "\n";
  } 
  std::cout << "After for loop." << std::endl;

  return 0;
}

int main(int argc, char* argv[])
{
  break_statement();
  return 0;
}

