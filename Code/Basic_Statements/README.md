+++ To compile the code use

g++ --std=c++17 -o if_statement if_statement.cxx
g++ --std=c++17 -o if_else_statement if_else_statement.cxx
g++ --std=c++17 -o while_loop while_loop.cxx
g++ --std=c++17 -o do_while_loop do_while_loop.cxx
g++ --std=c++17 -o for_loop for_loop.cxx
g++ --std=c++17 -o continue_statement continue_statement.cxx
g++ --std=c++17 -o break_statement break_statement.cxx

+++ To execute the code using ROOT

root if_statement.cxx
root if_else_statement.cxx
root while_loop.cxx
root do_while_loop.cxx
root for.loop.cxx
root continue_statement.cxx
root break_statement.cxx