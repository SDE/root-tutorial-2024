#include <iostream>

int continue_statement() 
{
  int a{20};

  std::cout << "Before for loop." << std::endl;
  for (int loop_counter = 0; loop_counter <= a; ++loop_counter) {
    // % is the rest of a integer division
    // loop_counter%2 is zero for even numbers which are skipped
    if ( loop_counter%2 == 0) continue;
    std::cout << "loop counter is " << loop_counter << "\n";
  } 
  std::cout << "After for loop." << std::endl;

  return 0;
}

int continue_statement1() 
{
  int a{20};
  int loop_counter{0};

  std::cout << "Before while loop." << std::endl;
  while (loop_counter <= a) {
    ++loop_counter;
    if ( loop_counter%2 == 0) continue;
    std::cout << "loop counter is " << loop_counter << "\n";
  } 
  std::cout << "After while loop." << std::endl;

  return 0;
}


int main(int argc, char* argv[])
{
  continue_statement();
  continue_statement1();
  return 0;
}

