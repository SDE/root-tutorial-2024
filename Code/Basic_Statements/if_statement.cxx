#include <iostream>

int if_statement() 
{
  int a{5};

  std::cout << "Before if statement." << std::endl;
  if (5 == a) {
    std::cout << "The variable a is equal to 5.\n";
  } 
  std::cout << "After if statement." << std::endl;

  return 0;
}

int main(int argc, char* argv[])
{
  if_statement();
  return 0;
}

