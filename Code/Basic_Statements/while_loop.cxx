#include <iostream>

int while_loop() 
{
  int a{5};
  int loop_counter{0};

  std::cout << "Before while loop." << std::endl;
  while (loop_counter <= a) {
    std::cout << "loop counter is " << loop_counter << "\n";
    ++loop_counter;
  } 
  std::cout << "After while loop." << std::endl;

  return 0;
}

int main(int argc, char* argv[])
{
  while_loop();
  return 0;
}

