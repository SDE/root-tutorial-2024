#include <iostream>

int allocateOnHeap(int array_size)
{
    std::cout << "Allocating an array with size " << array_size << "\n";

    int* myStack = new int[array_size];

    int usedMemory = sizeof myStack;
    std::cout << "The size of the array is " << usedMemory << " byte\n"; 

    auto pp = std::addressof(myStack);

    std::cout << "The pointer points to the address " << myStack << "\n"
              << "The address of the pointer is " << pp << std::endl; 



    myStack[0]=1;
    std::cout << "hi: " << myStack[0] << std::endl; // we'll use myStack[0] here so the compiler won't optimize the array away

    delete [] myStack;
    return 0;
}

int main()
{
  allocateOnHeap(1);
  allocateOnHeap(10000);
  allocateOnHeap(10000000);
  return 0;
}