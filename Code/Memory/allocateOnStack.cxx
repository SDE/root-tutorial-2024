#include <iostream>

int allocateOnStack(int array_size)
{
    std::cout << "Allocating an array with size " << array_size << "\n";

    int myStack[array_size];

    int usedMemory = sizeof myStack;
    std::cout << "The size of the array is " << usedMemory << " byte\n"; 

    myStack[0]=1;
    std::cout << "hi: " << myStack[0] << std::endl; // we'll use myStack[0] here so the compiler won't optimize the array away

    return 0;
}

int main()
{
  allocateOnStack(1);
  allocateOnStack(10000);
  allocateOnStack(10000000);
  return 0;
}