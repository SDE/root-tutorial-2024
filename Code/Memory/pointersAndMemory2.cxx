#include <cstdint>
#include <iostream>
#include <utility> 

std::pair<int*,int*> pointersAndMemory(int val1, int val2, bool delete_ptr=true)
{
  int* c = new int(val1);
  int* d = new int(val2);

  std::cout << "The raw pointer c points to the address " << c
            << " which has the value " << *c << std::endl; 

  std::cout << "The raw pointer d points to the address " << d
            << " which has the value " << *d << std::endl; 

  std:: cout << std::endl;

  if (delete_ptr) {
    delete c;
    delete d;
    std::pair <int*,int*> retVal (nullptr,nullptr); 
    return retVal;
  }
  else {
    std::pair <int*,int*> retVal (c,d); 
    return retVal;
  }
}

int main() 
{

  std::cout << "*** First call without deleting pointers *** " << std::endl;
   std::pair <int*,int*> addresses = pointersAndMemory(0, 6576,false);
  
  std::cout << "*** Second call with deleting pointers *** " << std::endl;
  pointersAndMemory(1,333);

  std::cout << "*** Third call with deleting pointers *** " << std::endl;
  pointersAndMemory(2,111);

  std::cout << "The pointer pointing to the address " << addresses.first
            << " has the value " << *addresses.first << std::endl; 

  std::cout << "The pointer pointing to the address " << addresses.second
            << " has the value " << *addresses.second << std::endl; 
}

