#include <iostream>

int variablesAndMemory(int val1, int val2)
{
  int a=val1;
  int b{val2};

  std::cout << "The variable a has the value " << a 
            << " and is stored at the address " << &a << std::endl; 

  std::cout << "The variable b has the value " << b 
            << " and is stored at the address " << &b << std::endl; 

  std::cout << std::endl;

  return 0;
}

int main() 
{
  std::cout << "*** First call *** " << std::endl; 
  variablesAndMemory(0, 6575);

  std::cout << "*** Second call *** " << std::endl; 
  variablesAndMemory(1, 333);
  return 0;
}

