# Information

The code in this directory is ment to be compiled to get an idea about the
memory management of C++.
Probably it can also be executed with the ROOT interpreter but this wasn't
tested. 

## Compile commands in GSI environment

```
g++ --std=c++17 allocateOnStack.cxx -o allocateOnStack
g++ --std=c++17 variablesAndMemory.cxx -o variablesAndMemory
g++ --std=c++17 allocateOnHeap.cxx -o allocateOnHeap
g++ --std=c++17 pointersAndMemory.cxx -o pointersAndMemory
g++ --std=c++17 STLContainersAndMemory.cxx -o STLContainersAndMemory
g++ --std=c++17 uniquePointersAndMemory.cxx	-o uniquePointersAndMemory
g++ --std=c++17 memory.cxx -o memory
```

## Find memory leaks using the tool valgrind

### Installing valgrind if not yet installed

If you are using the VM prepared for the tutorial you need to install the
tool valgrind. In Debian based systems like Ubuntu this is done unsing the
tool apt

`sudo apt install valgrind`

The additionla command `sudo` is needed to get administrator rights since
only an administrator is allowed to install additional packages.

If you are using a different environment than the VM check if valgrind is
installed. Simply type the command and check the printout. If it similar to

`valgrind: command not found`

the tool isn't installed.

Install the tool using the package manager of your distribution.

### Compile with debug information

The output of valgrind is much more usefull when debug information are added
to the executable. In this case the tool will printout exactely the line
where the problem appears.

`g++ -g --std=c++17 pointersAndMemory.cxx -o pointersAndMemory`

### Run the program with valgrind

Using the tool is rather easy, one needs to execute valgrind with some
parameters plus the path to the executable one wants to check.

`valgrind --leak-check=full ./pointersAndMemeory`

Running the small test program isn't any problem but running real large
execuatble slows down the execution of the executable enormously. The
documentation on https://valgrind.org says that the execution speed is
slower by a factor of 10-50.
