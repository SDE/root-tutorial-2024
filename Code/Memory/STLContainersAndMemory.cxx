#include <iostream>
#include <vector>

int STLContainersAndMemory(int val)
{
  std::vector<int> b{val};

  std::cout << "The vector has " << b.size() << " element." << std::endl;
  std::cout << "The address of the vector b is " << &b << std::endl; 
  std::cout << "The data stored at " << b.data() << std::endl;

  b.push_back(val+1);

  std::cout << "After adding one number the vector has " << b.size() << " elements." << std::endl; 
  std::cout << "The address of the vector b is " << &b << std::endl; 
  std::cout << "The data stored at " << b.data() << std::endl;

  b.push_back(val+2);

  std::cout << "After adding one number the vector has " << b.size() << " elements." << std::endl; 
  std::cout << "The address of the vector b is " << &b << std::endl; 
  std::cout << "The data stored at " << b.data() << std::endl;

/*
  std::cout << "The value of the first element of vector b is " << b[1]
            << std::endl;
*/
  std::cout << "The value of the first element of vector b is " << b.at(0)
            << std::endl;

  std::cout << std::endl;

  return 0;
}

int main() 
{
  std::cout << "*** First call ***" << std::endl;
  STLContainersAndMemory(6575);

  std::cout << "*** First call ***" << std::endl;
  STLContainersAndMemory(111);

  return 0;
}

