#include <TMath.h>

#include <iostream>

using std::cout;
using std::endl;

void CalcVolume() 
{
  double radius[5] = {5, 3.5, 25., 10., 9.76543};
  double height[5] = {10., 8., 3., 9., 6.54378};

  for (int i=0; i<5;++i) {
    double area = TMath::Pi() * radius[i] * radius[i];
    double volume = area * height[i];
    double surface = (2 * area) + (2 * TMath::Pi() * radius[i] * height[i]);
    cout << "A can with a radius of " << radius[i] << " cm and a height of " 
         << height[i] << " cm has" << endl;
    cout << "Volume: "<< volume << endl; 
    cout << "Surface: "<< surface << endl; 
    cout << "------------------------------" << endl;
  }
}

int main() {

  CalcVolume();
  return 0;
}