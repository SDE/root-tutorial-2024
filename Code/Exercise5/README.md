# Information

The code in this directory can be executed as ROOT macro within ROOT but it
can also be compiled into a normal executable.
It is needed to setup the runtime environment first.

## Run as ROOT macro

root -l TNtuple_write.C
root -l TNtuple_read.C

## Compile commands in GSI environment

g++ --std=c++17 -I$ROOTSYS/include/root -L$ROOTSYS/lib -lCore -lTree -lRIO -lMathCore TNtuple_write.C -o TNtuple_write
g++ --std=c++17 -I$ROOTSYS/include/root -L$ROOTSYS/lib -lCore -lTree -lRIO TNtuple_read.C -o TNtuple_read

