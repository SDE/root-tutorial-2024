#include <TCanvas.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TStyle.h>

#include <iostream>
#include <memory>
#include <vector>

void TGraphExample3() 
{
//  std::unique_ptr<TGraphErrors> graph{new TGraphErrors("ExampleInput.txt", "%lg %lg %lg %lg")};
  TGraphErrors* graph{new TGraphErrors("ExampleInput.txt", "%lg %lg %lg %lg")};

  if (!graph) {
    std::cerr << "Error creating the graph" << std::endl;
    exit(1);
  }

  graph->SetTitle("Fake Measurement;distance [cm];count rate [1/s]");
  graph->SetMarkerStyle(kOpenCircle);
  graph->SetMarkerColor(kBlue);
  graph->SetLineColor(kBlue);

//  std::unique_ptr<TCanvas> mycanvas{new TCanvas()};
  TCanvas* mycanvas{new TCanvas()};
  if (!mycanvas) {
    std::cerr << "Error creating the canvas" << std::endl;
    exit(1);
  }

  graph->Draw("APE");

//  std::unique_ptr<TF1> linear_function{new TF1("Linear function", "[0]+x*[1]", .5, 10.5)};
//  std::unique_ptr<TF1> linear_function{new TF1("Linear function", "pol1", .5, 10.5)};

  TF1* linear_function = new TF1("Linear function", "[0]+x*[1]", .5, 10.5);
//  TF1* linear_function = new TF1("Linear function", "pol1", .5, 10.5);

  if (!linear_function) {
    std::cerr << "Error creating the function" << std::endl;
    exit(1);
  }

  linear_function->SetLineColor(kRed);
  linear_function->SetLineStyle(2);

//  graph->Fit(linear_function.get());   // when using unique_ptr
  graph->Fit(linear_function);  // when using raw pointer
  linear_function->Draw("Same");

  gStyle->SetOptFit();
  mycanvas->Update();

//  std::cout << "Press Enter to continue..." << std::endl;
//  std::cin.get();

}

int main()
{
  TGraphExample3();
  return 0;
}
