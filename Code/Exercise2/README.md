# Information

The code in this directory can be executed as ROOT macro within ROOT but it
can also be compiled into a normal executable.
It is needed to setup the runtime environment first.

When using unique pointers it is necessary to have a break point at the end
of the fucntion. Otherwise the created objects are automatically destroyed 
when leaving the scope of the function. Normally this isn't what is expected
since one would like to examine the created histograms.  

## Run as ROOT macro

root -l TGraphExample1.C
root -l TGraphExample2.C
root -l TGraphExample3.C

## Compile commands in GSI environment

g++ --std=c++17 -I$ROOTSYS/include -L$ROOTSYS/lib -lCore -lHist -lGpad TGraphExample1.C -o TGraphExample1
g++ --std=c++17 -I$ROOTSYS/include -L$ROOTSYS/lib -lCore -lHist -lGpad TGraphExample2.C -o TGraphExample2
g++ --std=c++17 -I$ROOTSYS/include -L$ROOTSYS/lib -lCore -lHist -lGpad TGraphExample3.C -o TGraphExample3
