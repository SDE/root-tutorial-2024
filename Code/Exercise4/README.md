# Information

The code in this directory can be executed as ROOT macro within ROOT but it
can also be compiled into a normal executable.
It is needed to setup the runtime environment first.

## Run as ROOT macro

root -l IoExample.C

## Compile commands in GSI environment

g++ --std=c++17 -I$ROOTSYS/include/root -L$ROOTSYS/lib -lCore -RIO IoExample.C -o IoExample

