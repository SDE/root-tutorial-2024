#!/bin/bash

# Setup file to set the correct environment to run ROOT
# in the IT computing room

distribution=$(lsb_release -is)
if [[ "$distribution" != "Debian" ]]; then
  echo "Only Debian is supported"
  return 42
fi  

releaseVersion=$(lsb_release -rs)

if [[ $releaseVersion -eq 8 ]]; then
  source /cvmfs/fairsoft.gsi.de/debian${releaseVersion}/fairsoft/apr21/bin/thisroot.sh
else
  source /cvmfs/fairsoft.gsi.de/debian${releaseVersion}/fairsoft/nov22p1/bin/thisroot.sh
fi
