# ROOT Tutorial

This tutorial is an introduction to the program package ROOT
(https://root.cern.ch). It covers the basic features of the program package
from starting up ROOT, doing some simple calculations up to more complex examples
like writing data to file as a TTree.
This tutorial is used for the GSI Summer Student Program 2024.

If you don't have ROOT installed on your computer and you don't want to
install the program the easiest way to try it yourself is to use a virtual
machine with all necessary programs already preinstalled.

The advantage of using a virtual machine is that it works for all major
operating systems (Windows, Linux and macosx) in the same way.

## Get *VirtualBox*

VirtualBox is a powerful virtualisation product for PCs. If not already
installed please install *VirtualBox* on your machine: [https://www.virtualbox.org](https://www.virtualbox.org)
Download the Program from [here](https://www.virtualbox.org/wiki/Downloads)
and install it.
For Windows and macosx you simply download the installer and execute it.
For the various Linux systems you can find detailed installation
instructions on the download page.

## Setup a virtual machine
- Download the prepared appliance from [here](https://sf.gsi.de/d/1aaa367a47524a6cb509/)
   - In the directory on the webserver you find an image for  Ubuntu version 22.04. 
   - Due to the large size of the images the download can take some time.
- Import the appliance into *VirtualBox*
   - Start the VirtualBox application
   - From the menu "File” select “Import Appliance” and select the just downloaded file
   - The Machine is rather minimal (2 Cores, 4GB memory) but you can adjust the settings.
   - Boot the machine by double clinking on the available images
- Login to the virtual machine using the username/password combination: fairroot/FairRoot

---

## Instructions within in the virtual machine
- Get the all the files and material to your work area
   - Open a terminal using the terminal desktop icon
   - Execute the following command on the terminal

    `git clone https://git.gsi.de/SDE/root-tutorial-2024.git`

## Start ROOT
- Set the correct environment by typing `source /opt/root/bin/thisroot.sh`
   - This step must not be done since it is already put in the .bashrc login
   script such that it is executed automatically when opening a new
   window. It also doesn't hurt to execute it again.
- Start ROOT by typing `root` in a terminal

## Contact

Florian Uhlig, f.uhlig@gsi.de
Radoslaw Karabowicz, r.karabowicz@gsi.de
Dmytro Kresan, d.kresan@gsi.de
